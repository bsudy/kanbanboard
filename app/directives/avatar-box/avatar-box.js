'use strict';

var avatarBoxModule = angular.module('kbAvatarBox', ['kbServices']);
avatarBoxModule.directive('kbAvatarBox', function(userService) {
           //['$rootScope', '$parse', function ($rootScope, $parse) {
    return {
        restrict: 'E',
        transclude: true,
        controller: function ($scope, $parse, $element, $attrs, userService) {
            var onRemoveCallback = $parse($attrs.kbRemove) || null;
            $scope.$watch($attrs.kbUser, function (newVal, oldVal) {
                $scope.kbUser = newVal;
            });
            if ($attrs.kbIndex) {
                $scope.$watch($attrs.kbIndex, function(newVal) {
                    $scope.kbIndex = newVal;
                    if (newVal == $attrs.kbIndexMax) {
                        $scope.kbIndexClass = "warning";
                    } else if (newVal > $attrs.kbIndexMax) {
                        $scope.kbIndexClass = "overload";
                    } else {
                        $scope.kbIndexClass = null;
                    }
                });
            }
            if ($attrs.kbUserId) {
                $scope.$watch($attrs.kbUserId, function(newVal, oldVal) {
                    var user = userService.getById(newVal);
                    $scope.kbUser = user;
                });
            }
            $scope.onRemove = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $event.stopImmediatePropagation();
                if(!onRemoveCallback)return;
                onRemoveCallback($scope, {$data: $scope.kbUser, $event: $event});
            };
            $scope.removable = $attrs.kbRemove ? true :false;
            $scope.showIndex = $attrs.kbIndex ? true :false;
            var removeButton = $element.find("span");
            var _pressEvents = 'touchstart mousedown';
            removeButton.on(_pressEvents, function(event) {
                event.preventDefault();
                event.stopPropagation();
            });
        },
        templateUrl: 'app/directives/avatar-box/avatar-box.html'
    };
});