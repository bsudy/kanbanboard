'use strict';

var stickerModule = angular.module('kbSticker', []);
stickerModule.directive('kbSticker', function() {
    return {
        restrict: 'E',
        controller: function($scope, $parse, $attrs, $element) {
            var onRemoveCallback = $parse($attrs.kbRemove) || null;
            $scope.onRemove = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $event.stopImmediatePropagation();
                console.log("Remove: ", $event);
                if(!onRemoveCallback)return;
                onRemoveCallback($scope, {$data: $scope.label, $event: $event});
            };
            $scope.$watch($attrs.kbLabel, function (newVal, oldVal) {
                $scope.label = newVal;
            });
            
            $scope.removable = $attrs.kbRemove ? true :false;
            var removeButton = $element.find("span");
            var _pressEvents = 'touchstart mousedown';
            removeButton.on(_pressEvents, function(event) {
                event.preventDefault();
                event.stopPropagation();
            });   
        },
        templateUrl: 'app/directives/sticker/sticker.html'
    };
});