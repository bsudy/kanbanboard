(function () {
    'use strict';
    
    var firebaseRef = new Firebase("https://kboard.firebaseio.com/");

    var servicesModule = angular.module('kbServices', ['firebase']);
    servicesModule.factory('userService', function($filter, $http, $firebase) {
        var $service = {};
        var usersRef = firebaseRef.child("/users");
        var sync = $firebase(usersRef);
        $service.users = [];
        firebaseRef.onAuth(function(authData) {
            if (authData) {
                $service.users = sync.$asArray();
            } else {
                $service.users = [];
            }
        });
        $service.getById = function(userId) {
            if (!userId) return;
            var user = $filter('filter')($service.users, {id: userId});
            return user[0];
        };
        $service.getAll = function() {
            return $service.users;
        };
        return $service;
        
    });
    servicesModule.factory('storyService', ['$filter', '$http', '$firebase', function($filter, $http, $firebase) {
        var $service = {};
        
        var stroiesRef = firebaseRef.child("/stories");
        var sync = $firebase(stroiesRef);
        $service.stories = [];
        firebaseRef.onAuth(function(authData) {
            if (authData) {
                $service.stories = sync.$asArray();
            } else {
                $service.stories = [];
            }
        });
        
        $service.getAll = function() {
            return $service.stories;
        };
        $service.addUser = function(story, user) {
            if (story.assignees) {
                if (story.assignees.indexOf(user.id) < 0) {
                    story.assignees.push(user.id);
                    $service.stories.$save(story);
                }
            } else {
                story.assignees = [ user.id ];
                $service.stories.$save(story);
            }
        };
        $service.removeUser = function(story, user) {
            var index = story.assignees.indexOf(user.id);
            if (index > -1) {
                story.assignees.splice(index, 1);
                $service.stories.$save(story);
            }
        };
        $service.addLabel = function(story, label) {
            if (story.labels) {
                if (story.labels.indexOf(label) < 0) {
                    story.labels.push(label);
                    $service.stories.$save(story);
                }
            } else {
                story.labels = [ label ];
                $service.stories.$save(story);
            }
        };
        $service.removeLabel = function(story, label) {
            var index = story.labels.indexOf(label);

            if (index > -1) {
                story.labels.splice(index, 1);
                $service.stories.$save(story);
            }
        };
        $service.update = function(story) {
            $service.stories.$save(story);
        };
        return $service;
    }]);
    servicesModule.service("authService", ["$firebaseSimpleLogin", function($firebaseSimpleLogin) {
        var $service = {};
        
        var loginObj = $firebaseSimpleLogin(firebaseRef);
        $service.loginObj = loginObj;
        $service.user = loginObj.user;
        firebaseRef.onAuth(function(authData) {
            $service.user = authData ? authData.auth : null;
        });
        $service.login = function() {
            return loginObj.$login("google").then(function(user) {
                console.log("Logged in as: ", user);
            }, function(error) {
                console.error("Login failed: ", error);
            });
        };
        $service.logout = function() {
            loginObj.$logout();
        };
        return $service;
    }]);
    servicesModule.factory("simpleLogin", ["$firebaseSimpleLogin", function($firebaseSimpleLogin) {
        return $firebaseSimpleLogin(firebaseRef);
    }]);
}());