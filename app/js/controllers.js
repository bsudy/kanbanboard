'use strict';

var kanbanBoardControllers = angular.module('kanbanBoardControllers', ['kbServices']);

kanbanBoardControllers.controller('developerViewController', ['$scope', '$filter', '$location', 'userService', 'storyService', 'simpleLogin', 'currentUser',    
    function ($scope, $filter, $location, userService, storyService, simpleLogin, currentUser) {
//        if (currentUser === null) {
//            $location.path('/login');
//        }
        $scope.$watch(currentUser, function(newValue) {
            console.log("CurrentUser has been changed", newValue);
////            if (!currentUser) {
////                $location.path('/login');
////            }
        });
        $scope.currentUser = currentUser;
        $scope.columns = [
            {
                title: "Immediate backlog",
                status: "ReadyToStart",
                limit: 2,
                showAssignees: false
            },
            {
                title: "In Progress",
                status: "InProgress",
                limit: 3,
                showAssignees: true
            },
            {
                title: "Done",
                status: "Done",
                showAssignees: false
            }
        ];
        $scope.cards = [];
        $scope.users = [];
        $scope.labels = ["Code review", "To Close", "Bug"];
        $scope.$watch(userService.getAll, function(newData) {
            $scope.users = newData;
        });
        $scope.$watch(storyService.getAll, function(newData) {
            $scope.cards = newData;
        });
        $scope.onDragCard = function (data) {
            console.log("start", data);
        };
        $scope.onDragEnd = function (data, element) {
//            console.log("end", data);
        };
        $scope.onDragOver = function (data) {
//            console.log("over", data);
        };
        $scope.onDropToState = function (data, newState) {
            if (newState && data && data.type === 'story' && newState !== data.status) {
//                $scope.cards[$scope.cards.indexOf(data)].status = newState;
                data.status = newState;
                storyService.update(data);
            }
        };
        $scope.onDropToCard = function (data, card) {
            if (data && data.type === 'user') {
                storyService.addUser(card, data);
            } else if (data && data.type === 'label') {
                storyService.addLabel(card, data.label);
            }
        };
        $scope.removeUser = function(user, card) {
            storyService.removeUser(card, user);
        };
        $scope.removeLabel = function(label, card) {
            storyService.removeLabel(card, label);
        };
        $scope.onRemove = function($event, label, card) {
            $event.preventDefault();
            $event.stopPropagation();
            $event.stopImmediatePropagation();
            $scope.removeLabel(label, card);
        };
         
        $scope.auth = simpleLogin;
        $scope.loginObj = simpleLogin;
        $scope.user = simpleLogin.user;
        $scope.login = function() {
            simpleLogin.$login("google");
        };
        $scope.logout = function() {
            simpleLogin.$logout();
        };
    }
]);
kanbanBoardControllers.controller('loginViewController', ['$scope', '$filter', 'simpleLogin', '$location', 'authService',
    function ($scope, $filter, simpleLogin, $location, authService) {
        $scope.user = authService.loginObj.user;
        $scope.$watch(function() { return authService.user; }, function(newValue) {
            $scope.user = newValue;
        });
        $scope.login = function() {
            authService.login();
        };
        $scope.logout = function() {
            authService.logout();
        };
    }
]);