'use strict';

var kanbanBoardApp = angular.module('KanbanBoard', ['ngRoute', 'ngDraggable', 'ui.gravatar', 'kbAvatarBox', 'kbSticker',  'kanbanBoardControllers']);

kanbanBoardApp.config(['$routeProvider', '$logProvider',
    function ($routeProvider,$logProvider) {
     $logProvider.debugEnabled(true);
        $routeProvider.when('/dev', {
            templateUrl: 'app/partials/developer-view.html',
            controller: 'developerViewController',
            resolve: {
                "currentUser": ["simpleLogin", function(simpleLogin) {
                    return simpleLogin.$getCurrentUser();
                }]
            }
        }).when('/login', {
            templateUrl: 'app/partials/login-view.html',
            controller: 'loginViewController'
        }).otherwise({
            redirectTo: '/dev'
        });
    }
]);